#lang racket

(require web-server/servlet web-server/servlet-env)
(require "../server/servlets/translator-servlet.rkt")
;
; make background process: 'sudo -b nohup racket start-server.rkt'

(serve/servlet
	translator
	#:listen-ip #f ; #f - open to external http requests
	#:port 80  ; standard port
	; #:servlet-regexp #rx"" ; capture all top-level requests
	#:servlet-regexp #rx"/translate"
	#:extra-files-paths (list (build-path "../html"))
	#:launch-browser? #f
)
